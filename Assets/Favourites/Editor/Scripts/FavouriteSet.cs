﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace Roguenoodle.Favourites
{
    public class FavouriteSet : ScriptableObject
    {

        // Where to save new asset sets when the path in Preferences is invalid
        private const string AssetPath = "Assets/";

        // Default Set name
        private const string DefaultName = "Favourite Set";

        [SerializeField] private List<AssetFavourite> favourites;
        public List<AssetFavourite> Favourites
        {
            get 
            {
                if (favourites == null)
                {
                    favourites = new List<AssetFavourite> ();
                }
                return favourites;
            }
        }

        /// <summary>
        /// Create this instance.
        /// Returns the path of the created asset
        /// </summary>
        public static string Create ()
        {
            FavouriteSet newSet = CreateInstance<FavouriteSet>();

            // Check if preferences contains a valid path
            string path = Preferences.SavePath;
            if (!Directory.Exists(Preferences.SavePath))
            {
                Directory.CreateDirectory(path);
            }
            path += DefaultName + (".asset");

            path = AssetDatabase.GenerateUniqueAssetPath(path);
            AssetDatabase.CreateAsset(newSet, path);
            FavouritesWindow.RepaintWindow();
            return path;
        }

        public void AddFavourite (AssetFavourite favourite)
        {
            foreach (AssetFavourite fav in Favourites)
            {
                if (fav.GUID == favourite.GUID)
                {
                    return;
                }
            }
            Favourites.Add(favourite);
            FavouritesWindow.RepaintWindow();
            Save();
        }

        public void Save ()
        {
            EditorUtility.SetDirty(this);
            AssetDatabase.SaveAssets();
        }

        public void Draw ()
        {
            for (int i = 0; i < Favourites.Count; i++)
            {
                FavouriteDrawer.Draw(Favourites[i] as IFavourite);
            }
        }

        private void DrawDivider (int height)
        {
            Color cachedColor = GUI.backgroundColor;
            GUI.backgroundColor = Styles.AccentColor;
            Styles.PinDivider.fixedHeight = height;
            GUILayout.Box(GUIContent.none,Styles.PinDivider);
            GUI.backgroundColor = cachedColor;
        }

        public void RemoveQueuedFavourites ()
        {
            IEnumerable<AssetFavourite> queuedForRemoval = Favourites.Where(fav => fav.ShouldRemove);
            for (int i = 0; i < queuedForRemoval.Count(); i++)
            {
                Favourites.Remove(queuedForRemoval.ElementAt(i));
            } 
            FavouritesWindow.RepaintWindow();
            Save();
        }

    }
}

