﻿using UnityEngine;
using System.Collections;
using UnityEditor;
namespace Roguenoodle.Favourites
{
    public static class Preferences 
    {
        private static Color defaultAccentPro = new Color (0.3313726f, 0.7f, 0.6784314f);
        private static Color defaultAccentPE = new Color (0.176f, 0.48f, 0.65f);

        private static bool prefsLoaded = false;
        private static bool showTooltips = true;

        private static bool prefabsInstantiateAsChild = true;

        private static string savePath = "Assets/Favourites/Sets/";

        [PreferenceItem("Favourites")]
        private static void FavouritesPrefs ()
        {
            Color color = EditorGUIUtility.isProSkin ? GetColorWithKey("AccentColorPro") : GetColorWithKey("AccentColorPE");
            if (EditorGUIUtility.isProSkin)
            {
                color = EditorGUILayout.ColorField("Accent Color", color);
                if (GUI.changed)
                {
                    Styles.AccentColorPro = color;
                    SaveColor("AccentColorPro", color);
                    // save color here
                }
            }
            else
            {
                color = EditorGUILayout.ColorField("Accent Color", color);
                if (GUI.changed)
                { 
                    Styles.AccentColorPE = color;
                    SaveColor("AccentColorPE" ,color);
                    // save color here
                }
            }

            showTooltips = EditorGUILayout.Toggle("Show Tooltips", showTooltips);
            prefabsInstantiateAsChild = EditorGUILayout.Toggle("Prefab Instantiates as Child", prefabsInstantiateAsChild);
            savePath = EditorGUILayout.TextField("Folder for New Sets", savePath);
            if (GUI.changed)
            {
                EditorPrefs.SetBool("ShowTooltips", showTooltips);
                EditorPrefs.SetBool("PrefabInstantiatesAsChild", prefabsInstantiateAsChild);
                EditorPrefs.SetString("SavePath", savePath);
                Styles.UpdateTooltips();
            }

        }

        public static Color GetColorWithKey (string key)
        {
            Color defaultColor = (key == "AccentColorPro") ? defaultAccentPro : defaultAccentPE;
            return new Color(
                EditorPrefs.GetFloat(key + "_R", defaultColor.r),
                EditorPrefs.GetFloat(key + "_G", defaultColor.g),
                EditorPrefs.GetFloat(key + "_B", defaultColor.b),
                1f);

        }

        private static void SaveColor (string key, Color color)
        {
            EditorPrefs.SetFloat(key + "_R", color.r);
            EditorPrefs.SetFloat(key + "_G", color.g);
            EditorPrefs.SetFloat(key + "_B", color.b);
        }

        private static void LoadPrefs ()
        {
            showTooltips = EditorPrefs.GetBool("ShowTooltips", showTooltips);
            prefabsInstantiateAsChild = EditorPrefs.GetBool("PrefabInstantiatesAsChild", prefabsInstantiateAsChild);
            savePath = EditorPrefs.GetString("SavePath", savePath);
            prefsLoaded = true;
        }

        public static bool ShowTooltips
        {
            get
            {
                if (!prefsLoaded)
                {
                    LoadPrefs();
                }
                return showTooltips;
            }
        }

        public static string SavePath
        {
            get
            {
                if (!prefsLoaded)
                {
                    LoadPrefs();
                }
                return savePath;
            }
        }

        public static bool PrefabInstantiatesAsChild
        {
            get
            {
                if (!prefsLoaded)
                {
                    LoadPrefs();
                }
                return prefabsInstantiateAsChild;
            }
        }

    }
}


