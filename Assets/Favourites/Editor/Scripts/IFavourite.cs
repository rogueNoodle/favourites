﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

public interface IFavourite 
{

    bool IsPinned { get; set; }
    bool ShouldRemove { get; set; }

    float Height { get; }

    Texture MainIcon { get; }

    string MainLabel { get; }
    string SecondaryLabel { get; }
    string Extension { get; }

    void MainAction ();
    void SecondaryAction ();
    GUIContent SecondaryActionContent { get; }

    bool HasExtras { get; }
    GUIContent ToggleExtrasContent { get; }
    bool ShouldDrawExtras { get; set; }
    void DrawExtras ();

}
